
// Global killswitch: only run if we are in a supported browser
if (Drupal.jsEnabled) {
  $(document).ready(
  function(){
    $('a#comment').click(function(){
      var comment = function (data) {
        $('div.msg1').html(data).show("slow");
      }
      $.get(this.href, null, comment);
      return false;
    }
    );
  });
  $(document).ready(
  function(){
    $('a#copy_to_blog').click(function(){
      var copyToBlog = function (data) {
        $('div.msg2').html(data).show("slow");
      }
      $.get(this.href, null, copyToBlog); // copyToBlog is called when data returned successfully
      return false;
    });
  }
  );
  $(document).ready(
  function(){
    $('a#report').click(function(){
      var copyToReport = function (data) {
        $('div.msg').fadeIn('slow').html(result['msg']);
      }
      $.get(this.href, null, copyToReport);
      return false;
    }
    );
  }
  );
  $(document).ready(
  function(){
    $('a#refresh').click(function(){
      var refresh = function (data) {
        $('div#writing_count').html(data);
      }
      $.get(this.href, null, refresh);
      return false;
    }
    );
  }
  );
  $(document).ready(
  function(){
    $('#create_writings').click(function(){
      var pop = function (data) {
        $('#dummy_writing_message').html(data);
      }
      $.get(this.href, null, pop);
      return false;
    }
    );
  }
  );
  $(document).ready(
  function(){
    $('#start_assignment').click(function(){
      var start = function (data) {
        $('#start_message').html(data);
      }
      $.get(this.href, null, start);
      return false;
    }
    );
  }
  );
  $(document).ready(
  function(){
    $('a#refresh_review_count').click(function(){
      var start = function (data) {
        $('#review_count').html(data).show('slow');
      }
      $.get(this.href, null, start);
      return false;
    }
    );
  }
  );
}
